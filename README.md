# 🛸 Docker'd Angular

*Docker'd Angular* is a project template that helps developers to easily work with Angular and Docker together. 
After some work, I've got Docker to use the same user as the host's one. This way, no permission error should occur. 

I have created a bunch more files in order to make its usage more enjoyable. In particular:

- `.env` containes the required environmental variables.
    - `PROJECT_NAME` describes the project's name,
    - `PROJECT_IGNORE_FOLDERS` shall be an array of folders placed into  `$PROJECT_NAME` to be ignored by `git`, 
    - `FILE` is the file to check if a project already exists; in this case, the `package.json` is an essential one,
    - `STYLE` is the type of style your Angular project should work with. Feel free to choose accordingly to the Angular CLI's documentation,
    - `ANGULAR_USER` is the `uid` of the user used in the Docker virtual machine. This is vital for this template: since Docker's default user is `root`, modifying files within the Docker container would cause permission problems. Using the same `uid` resolves the issue. 

- `init.sh` is the first script you need to execute. If run with `./init.sh git` it reinitializes the git repo, deleting the previous one. Then, it removes any line of `.env` containing `ANGULAR_USER`. Once done, it updated the Dockerfile, inserting the proper `uid`, and modifies `.env` file, in order to provide to the Docker container which `uid` should it use. 

- `entrypoint.sh` it the script run on startup by the Docker container. It creates a new project if none is found, then serves it. 

- `Dockerfile` is used to build the container's image. It first downloads some packages: `npm` is used to run Angular, while `shadow` adds the `usermod` file which is necessary. Once Angular is installed and the *workdir* is set, Docker creates a new user `you` with no home directory and no password. It then sets the "home" folder to `/devel` so that any file created will be owned by `you`. Lastly, it changes the user's id, so that it can match host's. 

## 🗺️ Instructions

1. Create a valid `.env` file using `./init.sh`
```sh
./init.sh
```

2. Run `docker-compose`
```sh
docker compose up -d
```

## ⚠️ Attention!

When changing host machine, make sure to change the Dockerfile's modified user to the string `CHANGE_ME`. Otherwise, the user's id used in the Docker container could not match your host machine's one. Be aware of this aspect as this template is not able to adapt to different environments: tinkering is still needed!
