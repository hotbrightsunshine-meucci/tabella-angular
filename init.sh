#! /bin/bash

# Importing env variables
source .env

mkdir src
mkdir "src/$PROJECT_NAME" # Necessary!

# If user needs to recreate git repo 
if [ ! -z "$1" ] && [ $1 == "git" ];
then 

    # New Git repository
    rm -rf ./.git

    echo "src/.npm" > .gitignore

    # Ignoring folders inside src/$PROJECT_NAME
    for i in "${PROJECT_IGNORE_FOLDERS[@]}"
    do
        echo "src/$PROJECT_NAME/"$i >> .gitignore
    done

    git init 
    git add .
    git commit -am "first commit! (init.sh)"

fi

# Removes pre-existing line starting with $ANGULAR_USER
sed -i "/ANGULAR_USER/d" ./.env

sed -i "s/CHANGE_ME/$(id -u)/" Dockerfile

# Appends the string to .env
echo "ANGULAR_USER=$(id -u)" >> ./.env 