FROM alpine
USER root

RUN apk update
RUN apk add npm
RUN apk add shadow

RUN npm install -g @angular/cli
WORKDIR /devel

RUN adduser -HDS you
RUN usermod -d /devel you
RUN chown -R you /devel
RUN usermod -u 1001 you
COPY entrypoint.sh /cmd/

USER you

EXPOSE 4200/tcp
ENTRYPOINT /cmd/entrypoint.sh