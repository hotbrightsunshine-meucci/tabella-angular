import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, Observable, retry } from "rxjs";
import { Response } from "./employee.data";

@Injectable() 
export class EmployeeService {
    constructor(private http: HttpClient) {}

    getEmployees(page: number, size: number): Observable<Response> {
        return this.getData("http://localhost/employees?page=" 
            + page + "&size=" + size)
    }

    getData (url: string): Observable<Response> {
        return this.http.get<Response>(url)
            .pipe(retry(1));
    }
}
