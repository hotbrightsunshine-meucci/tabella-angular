export interface Embedded {
    employees: Employee[]
}

export interface Page {
    size: number,
    totalElements: number,
    totalPages: number,
    number: number
}

export interface Links {
    first: Link,
    self: Link,
    next: Link,
    last: Link,
    profile: Link
}

export interface Link {
    href: string,
    templated?: boolean
}

export interface Employee     {
    id: number,
    firstName: string,
    lastName: string
    birthDate: Date,
    gender: string,
    hireDate: Date,
}

export interface Response {
    _embedded: Embedded,
    _links: Links,
    page: Page
}