import { Component, OnInit } from '@angular/core';
import { Employee } from '../services/employee.data';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'my-table',
  templateUrl: './my-table.component.html',
  styleUrls: ['./my-table.component.css']
})
export class MyTableComponent implements OnInit {
    data: Employee[] | undefined;
    displayedColumns = ["id", "firstName", "lastName", "gender", "birthDate", "hireDate"];

    constructor(private service: EmployeeService ) {}

    ngOnInit() {
        this.service.getEmployees(1, 10).subscribe(
            data => this.data = data._embedded.employees,
        );
    }
}
