#! /bin/sh

cd /devel

if test ! -f "./$PROJECT_NAME/$FILE"; then
    ng new "$PROJECT_NAME" --skip-git --skip-tests --style "$STYLE" --package-manager npm
fi

cd "$PROJECT_NAME"

npm install

# With --host 0.0.0.0 I make the service available
# To the host machine.
ng serve --host 0.0.0.0
